var program = require("commander");
var fetch = require("node-fetch");

program
  .version("0.1.0")
  .option("-t, --top-headlines", "Search the top headlines")  // boolean
  .option("-s, --source [type]", "Add the source that the news will be fetched from", " ") // string
  .option("-m, --max-articles [type]", "Amount of articles that will be fetched", 10)
  .parse(process.argv);

const APIKEY = "ce1aca61cca5441db9e6d17a26fcce3a";

/* Function thst is used to present data in the right format */
const view = (json, noDescription, maxArticles, firstCall = true) => {
  const newLines = () => { console.log("--------------------"); };
  if (firstCall === true){
    newLines();
    console.log(`We Bring The News`);
    newLines();
  }
  maxArticles = Math.round(maxArticles);
  if (maxArticles < 1) maxArticles = 1;
  json.articles.splice(maxArticles);
  json.articles.forEach( (obj) => {
    console.log(`Title : ${obj.title}`);
    if (!noDescription) console.log(`Description : ${obj.description}`);
    console.log(`Author : ${obj.author}`);
    console.log(`Source : ${obj.source.name}`);
    newLines();
  })
};

/* Function that uses the config.json to bring the news */
const funNoSrc = (noDescription, arr) => {
  let firstCallView = true;
  arr.forEach( (obj) => {
    if ((obj.query !== undefined) && (obj.source !== undefined)){
      const URL = `https://newsapi.org/v2/top-headlines?q=${obj.query}&sources=${obj.source}&apiKey=${APIKEY}`;
      fetch(URL)
          .then(function(res) {
              return res.json();
          }).then(function(json) {
              if (json.status=="ok"){
                view(json, noDescription, obj.maxArticles, firstCallView);
                firstCallView = false;
              }else{
                console.log(`The source (${obj.source}) you entered is invalid :(`);
              }
          });
    }
  })
};

/* Funtion in case he sets a source site to bring the news */
const funYesSrc = (noDescription, maxArticles, sourceSite) => {
  const URL = `https://newsapi.org/v2/top-headlines?sources=${sourceSite}&apikey=${APIKEY}`;
  fetch(URL)
      .then(function(res) {
          return res.json();
      }).then(function(json) {
          if (json.status=="ok"){
            view(json, noDescription, maxArticles)
          }else{
            console.log("No such source is available :(");
          }
      });
};

/* Main Program */
if (program.source===" "){
  var config = require("./config.json");
  console.log("import config");
  funNoSrc(program.topHeadlines, config.keywords);
}else{
  funYesSrc(program.topHeadlines, program.maxArticles, program.source);
}
